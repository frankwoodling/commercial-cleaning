# Commercial Cleaning Marketing Site

## Overview

This project is a web application for a commercial cleaning service, built using React, TypeScript, and Vite. It provides a interface for prospective clients to learn about services, contact the company, and explore various cleaning solutions.

## Features

- **React 18**: Utilizes the latest version of React for a dynamic user experience
- **TypeScript**: Ensures type safety and improved developer experience
- **Vite**: Fast build tool that enhances development speed with hot module replacement
- **Mantine UI**: Leverages Mantine for a rich set of customizable UI components
- **Routing**: Uses React Router for seamless navigation between pages
- **Testing**: Configured with Vitest and React Testing Library for robust testing capabilities
- **Linting and Formatting**: ESLint and Prettier ensure code quality and consistency
- **PostCSS**: Utilizes PostCSS with Mantine preset for advanced CSS features
- **React Email and Resend**: Custom emails for contact form

## Available Scripts

### Frontend

- `npm run dev`: Starts the development server
- `npm run build`: Compiles TypeScript and builds the project for production
- `npm run preview`: Serves the built app for preview
- `npm run lint`: Runs ESLint for code quality checks
- `npm run prettier`: Formats code using Prettier
- `npm run test`: Runs type checking, linting, and testing
