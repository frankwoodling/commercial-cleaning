import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import { AppShell } from '@mantine/core';
import { useClickOutside, useDisclosure } from '@mantine/hooks';
import { ScrollToTop } from '@components/ScrollToTop/ScrollToTop.component';
import { Header } from './components/Header/Header.subcontainer';
import { MobileNav } from './components/MobileNav/MobileNav.subcontainer';

function App() {
  const [opened, { toggle }] = useDisclosure();

  // Set up click outside for mobile nav ignoring the burger/close button
  const [mobileRef, setMobileRef] = useState<HTMLDivElement | null>(null);
  const [burgerRef, setBurgerRef] = useState<HTMLDivElement | null>(null);

  useClickOutside(
    () => {
      if (opened) toggle();
    },
    null,
    [mobileRef, burgerRef],
  );

  return (
    <AppShell
      header={{ height: 80 }}
      navbar={{ width: 300, breakpoint: 'md', collapsed: { desktop: true, mobile: !opened } }}
    >
      <ScrollToTop />
      <Header isOpen={opened} toggleMobileNav={toggle} setRef={(ref) => setBurgerRef(ref as HTMLDivElement | null)} />
      <MobileNav toggleMobileNav={toggle} setRef={(ref) => setMobileRef(ref as HTMLDivElement | null)} />

      <AppShell.Main>
        <Outlet />
      </AppShell.Main>
    </AppShell>
  );
}

export default App;
