export enum RoutesEnum {
  Home = '/',
  Services = '/services',
  AboutUs = '/about-us',
  ContactUs = '/contact-us',
}
