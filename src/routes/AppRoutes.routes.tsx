import { createBrowserRouter } from 'react-router-dom';
import { NotFound } from '@pages/NotFound/NotFound.page';
import { Services } from '@pages/Services/Services.page';
import App from '../App';
import { About } from '../pages/About/About.page';
import { Contact } from '../pages/Contact/Contact.page';
import { Landing } from '../pages/Landing/Landing.page';
import { RoutesEnum } from './AppRoutes.types';

export const appRoutes = createBrowserRouter(
  [
    {
      path: RoutesEnum.Home,
      element: <App />,
      errorElement: <App />,
      children: [
        {
          path: '',
          element: <Landing />,
        },
        {
          path: 'services',
          element: <Services />,
        },
        {
          path: RoutesEnum.Services,
          element: <Services />,
        },
        {
          path: RoutesEnum.AboutUs,
          element: <About />,
        },
        {
          path: RoutesEnum.ContactUs,
          element: <Contact />,
        },
        {
          path: '*',
          element: <NotFound />,
        },
      ],
    },
  ],
  {
    basename: RoutesEnum.Home,
  },
);
