import { SimpleGrid, Stack, Text, Title } from '@mantine/core';
import { ServiceDetailCard, ServiceDetailCardProps } from '@components/ServiceDetailCard/ServiceDetailCard.component';
import { COMPANY_NAME } from '@constants/company.constants';
import styles from './Services.module.css';

const cardData: ServiceDetailCardProps[] = [
  {
    title: 'General Commercial Cleaning',
    description:
      'Ensure your commercial spaces reflect professionalism with our routine cleaning services. Ideal for maintaining daily cleanliness and a polished appearance, our team focuses on high-traffic areas to keep your environment pristine and welcoming.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721544257/commercial_cleaning/services_general_commercial_v1_z0s5mv.png',
    badges: [{ emoji: '🌟', label: 'Spotless Maintenance' }],
  },
  {
    title: 'Deep Commercial Cleaning',
    description:
      'Target deep-seated dirt and pathogens with our comprehensive deep cleaning services. Perfect for medical facilities, schools, and high-traffic areas needing extra care, we use advanced techniques and eco-friendly products to ensure a thorough clean.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721697719/commercial_cleaning/services_deep_clean_v1_pdzxsi.png',
    badges: [{ emoji: '🔍', label: 'Intensive Clean' }],
  },
  {
    title: 'Post-Construction Cleaning',
    description:
      'Our post-construction cleaning services are tailored to meet the specific needs of your project at various stages: Rough Clean, focusing on removing large debris and sweeping or vacuuming surfaces before flooring is installed; Final Clean, ensuring all finishes are installed and the unit is move-in ready; and Light Clean, designed to remove residual dust and soil on surfaces or any marks from punch list activities. Each service can be requested individually or combined according to your project requirements.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721544869/commercial_cleaning/services_construction_v1_rasthd.png',
    badges: [
      { emoji: '🚧', label: 'Rough Clean' },
      { emoji: '🌟', label: 'Final Clean' },
      { emoji: '🧹', label: 'Light Clean' },
    ],
  },
  {
    title: 'Janitorial Services',
    description:
      'Regular janitorial services to keep your facilities clean, safe, and welcoming. Custom schedules and tasks to fit your needs, focusing on areas that require the most care to maintain a healthy environment for everyone.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721627894/commercial_cleaning/services_janitorial_v1_fcrocx.png',
    badges: [{ emoji: '🛡️', label: 'Reliable Upkeep' }],
  },
  {
    title: 'Post-Event Cleaning',
    description:
      'Efficient and thorough cleaning services to quickly reset your venue after events, ensuring it remains pristine for the next gathering. Our team handles everything from waste disposal to surface sanitization, allowing you to focus on hosting without the hassle.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721628811/commercial_cleaning/services_event_v1_skan9u.png',
    badges: [{ emoji: '🎈', label: 'Event Ready' }],
  },
  {
    title: 'Move-in Office Cleaning',
    description:
      'Detailed cleaning to prepare your office for move-in day. We ensure a fresh, clean start in your new workspace, addressing areas like carpets, windows, and high-touch surfaces to create a welcoming and safe environment.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721681294/commercial_cleaning/services_move_in_v1_fmmuwe.png',
    badges: [{ emoji: '🚚', label: 'Welcome Clean' }],
  },
  {
    title: 'Move-out Office Cleaning',
    description:
      'Comprehensive cleaning after you move out to ensure the space is spotless and ready for the next occupants. Our detailed approach includes deep cleaning of all areas, ensuring that every corner of the office is immaculate.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721851797/commercial_cleaning/services_move_out_v1_d5mjwa.png',
    badges: [{ emoji: '🧹', label: 'Complete Clear-Out' }],
  },
  {
    title: 'Apartment Turnovers',
    description:
      'Complete cleaning solutions for apartment turnovers, ensuring each unit is immaculately ready for new tenants. From deep cleaning of floors and fixtures to dusting and polishing surfaces, we handle all aspects to enhance tenant satisfaction.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721694470/commercial_cleaning/services_apartment_v1_jngccu.png',
    badges: [{ emoji: '🔑', label: 'Tenant Transition' }],
  },
  {
    title: 'Custom Cleaning Solutions',
    description:
      'Tailored cleaning services designed to meet the specific demands of any client or industry, with flexible options to suit any requirement. Whether it’s adapting to unique space layouts or using specialized cleaning agents, our solutions are crafted to your needs.',
    image:
      'https://res.cloudinary.com/dwrctul3s/image/upload/v1721685181/commercial_cleaning/services_custom_v1_kwvqpu.png',
    badges: [{ emoji: '🛠️', label: 'Tailored Perfection' }],
  },
];

export const ServiceDetailsListingSection = () => {
  const cards = cardData.map((card, index) => (
    <ServiceDetailCard
      key={index}
      title={card.title}
      description={card.description}
      image={card.image}
      badges={card.badges}
    />
  ));

  return (
    <section className={styles.serviceListingSection}>
      <Stack className={styles.serviceListingHeading} align="center">
        <Title order={1}>Commercial Cleaning Services We Offer</Title>
        <Text className={styles.serviceListingDescription} size="lg" c="dimmed">
          At {COMPANY_NAME}, we understand that the cleanliness of your commercial space is crucial to your business's
          image and operations. That's why we offer a wide range of specialized cleaning services designed to meet every
          need of your bustling environment. From routine janitorial work to deep cleaning and post-construction
          clean-ups, our expert team is equipped with the latest tools and eco-friendly products to deliver impeccable
          results.
          <br />
          <br />
          Whether you're preparing for a corporate move, recovering from a major event, or simply maintaining day-to-day
          cleanliness, our tailored solutions are here to ensure your space is not only spotless but also healthy and
          inviting. Dive into our diverse offerings and discover how we can transform your commercial spaces into
          pristine business environments.
        </Text>
      </Stack>
      <SimpleGrid className={styles.serviceListingCards} cols={{ base: 1, md: 2 }}>
        {cards}
      </SimpleGrid>
    </section>
  );
};
