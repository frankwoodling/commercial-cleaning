import { Container } from '@mantine/core';
import { Footer } from '@components/Footer/Footer.component';
import { Hero } from '@components/Hero/Hero.component';
import {
  HeroButtons,
  HeroDescriptions,
  HeroImages,
  HeroLinks,
  HeroPills,
  HeroTitles,
} from '@components/Hero/Hero.types';
import { ServiceDetailsListingSection } from './ServiceDetailsListingSection.subcontainer';
import styles from './Services.module.css';

export const Services = () => {
  return (
    <Container size="xl" className={styles.services}>
      <Hero
        pillText={HeroPills.Services}
        titleText={HeroTitles.Services}
        description={HeroDescriptions.Services}
        buttonText={HeroButtons.Services}
        buttonLink={HeroLinks.Services}
        imageUrl={HeroImages.Services}
      />

      <ServiceDetailsListingSection />
      <Footer />
    </Container>
  );
};
