import { BiBuildingHouse, BiPencil } from 'react-icons/bi';
import { HiOutlineOfficeBuilding } from 'react-icons/hi';
import { LiaBroomSolid } from 'react-icons/lia';
import { MdOutlineCleaningServices } from 'react-icons/md';
import { PiBuildingsFill, PiBuildingsLight } from 'react-icons/pi';
import { SlWrench } from 'react-icons/sl';
import { TbBuildingCircus } from 'react-icons/tb';
import { SimpleGrid, Stack, Text, Title } from '@mantine/core';
import {
  ServiceSummaryCard,
  ServiceSummaryCardProps,
} from '../../components/ServiceSummaryCard/ServiceSummaryCard.component';
import styles from './Landing.module.css';

const cardData: ServiceSummaryCardProps[] = [
  {
    title: 'General Commercial Cleaning',
    description: 'Routine cleaning services tailored to maintain the professionalism of your commercial space.',
    icon: <HiOutlineOfficeBuilding />,
  },
  {
    title: 'Deep Commercial Cleaning',
    description:
      'Comprehensive deep cleaning that eradicates deep-seated dirt, grime, and pathogens for a healthier environment.',
    icon: <MdOutlineCleaningServices />,
  },
  {
    title: 'Post-Construction Cleaning',
    description:
      'Efficient removal of construction debris and dust, preparing your space for its new beginning with meticulous attention to detail.',
    icon: <SlWrench />,
  },
  {
    title: 'Janitorial Services',
    description: 'Regular maintenance cleaning to ensure your facilities are always clean, safe, and welcoming.',
    icon: <LiaBroomSolid />,
  },
  {
    title: 'Post-Event Cleaning',
    description:
      'Quick and thorough cleaning services to reset your venue, ensuring it is pristine for the next event.',
    icon: <TbBuildingCircus />,
  },
  {
    title: 'Move-in Office Cleaning',
    description: 'Detailed cleaning to prepare your new office space for move-in day, ensuring a fresh start.',
    icon: <PiBuildingsLight />,
  },
  {
    title: 'Move-out Office Cleaning',
    description: 'Thorough cleaning post-move-out to leave your office space spotless for the next occupants.',
    icon: <PiBuildingsFill />,
  },
  {
    title: 'Apartment Turnovers',
    description: 'Complete cleaning solutions for apartment turnovers, readying each unit for new tenants.',
    icon: <BiBuildingHouse />,
  },
  {
    title: 'Custom Cleaning Solutions',
    description: 'Flexible cleaning services designed to meet the unique demands of any client or industry.',
    icon: <BiPencil />,
  },
];

export const ServiceSummarySection = () => {
  const cards = cardData.map((card, index) => (
    <ServiceSummaryCard key={index} title={card.title} description={card.description} icon={card.icon} />
  ));

  return (
    <section className={styles.serviceSummarySection}>
      <Stack className={styles.serviceSummaryHeading} align="center">
        <Title order={1}>Commercial Cleaning Services We Offer</Title>
        <Text className={styles.serviceSummaryDescription} size="lg" c="dimmed">
          Whether you need routine upkeep or specialized cleaning, our services are designed to meet the diverse needs
          of various industries and spaces.
        </Text>
      </Stack>
      <SimpleGrid className={styles.serviceSummaryCards} cols={{ base: 1, md: 3 }}>
        {cards}
      </SimpleGrid>
    </section>
  );
};
