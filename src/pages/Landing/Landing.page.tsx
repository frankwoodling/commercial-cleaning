import { Container } from '@mantine/core';
import { FeatureBannerImages } from '@components/FeatureBanner/FeatureBanner.config';
import { FeatureBanner } from '@components/FeatureBanner/FeatureBanner.subcomponent';
import { Hero } from '@components/Hero/Hero.component';
import { Footer } from '../../components/Footer/Footer.component';
import {
  HeroButtons,
  HeroDescriptions,
  HeroImages,
  HeroLinks,
  HeroPills,
  HeroTitles,
} from '../../components/Hero/Hero.types';
import { ListCard } from '../../components/ListCard/ListCard.subcontainer';
import { BuildingTypesSummarySection } from './BuildingTypesSummarySection.subcontainer';
import styles from './Landing.module.css';
import { ServiceSummarySection } from './ServiceSummarySection.subcontainer';

export const Landing = () => {
  return (
    <Container size="xl" className={styles.landing}>
      <Hero
        pillText={HeroPills.Landing}
        titleText={HeroTitles.Landing}
        description={HeroDescriptions.Landing}
        buttonText={HeroButtons.Landing}
        buttonLink={HeroLinks.Landing}
        imageUrl={HeroImages.Landing}
      />
      <FeatureBanner content={<ListCard type="landing" />} imageUrl={FeatureBannerImages.Landing} />
      <ServiceSummarySection />
      <BuildingTypesSummarySection />
      <Footer />
    </Container>
  );
};
