import { AiOutlineBank } from 'react-icons/ai';
import { BiBuilding, BiBuildingHouse, BiStore } from 'react-icons/bi';
import { CgGym } from 'react-icons/cg';
import { FaChildren } from 'react-icons/fa6';
import { LiaCarSideSolid, LiaPlaceOfWorshipSolid } from 'react-icons/lia';
import { MdOutlineConstruction, MdOutlineTheaters } from 'react-icons/md';
import { TbBuildingCircus, TbSchool } from 'react-icons/tb';
import { SimpleGrid, Stack, Text, Title } from '@mantine/core';
import {
  ServiceSummaryCard,
  ServiceSummaryCardProps,
} from '../../components/ServiceSummaryCard/ServiceSummaryCard.component';
import styles from './Landing.module.css';

const cardData: ServiceSummaryCardProps[] = [
  {
    title: 'Offices',
    description: 'Enhanced cleaning for every workspace, ensuring a polished and productive environment.',
    icon: <BiBuilding />,
  },
  {
    title: 'Auto Dealerships',
    description: 'Showroom-ready cleaning that drives customer satisfaction and sales.',
    icon: <LiaCarSideSolid />,
  },
  {
    title: 'Educational Facilities',
    description: 'Secure and sanitized spaces for educational excellence.',
    icon: <TbSchool />,
  },
  {
    title: 'Daycares',
    description: 'Gentle, safe, and thorough cleaning for the care of our youngest.',
    icon: <FaChildren />,
  },
  {
    title: 'Fitness Centers',
    description: 'Energizing cleaning services that keep your fitness spaces healthy and inviting.',
    icon: <CgGym />,
  },
  {
    title: 'Retail Businesses',
    description: 'Spotless cleaning to enhance the retail experience and customer journey.',
    icon: <BiStore />,
  },
  {
    title: 'Apartments',
    description: 'Detailed cleaning for every unit, ensuring a welcoming home for all residents.',
    icon: <BiBuildingHouse />,
  },
  {
    title: 'Places of Worship',
    description: 'Reverent cleaning services that respect and enhance sacred spaces.',
    icon: <LiaPlaceOfWorshipSolid />,
  },
  {
    title: 'Financial Institutions',
    description: 'Trust-enhancing cleaning services for financial environments.',
    icon: <AiOutlineBank />,
  },
  {
    title: 'Event Centers',
    description: 'Event-ready cleaning that sets the stage for memorable experiences.',
    icon: <TbBuildingCircus />,
  },
  {
    title: 'Recreational Facilities',
    description: 'Dynamic cleaning solutions for spaces dedicated to recreation and leisure.',
    icon: <MdOutlineTheaters />,
  },
  {
    title: 'Post Construction',
    description: 'Meticulous post-construction cleaning to make your new space shine.',
    icon: <MdOutlineConstruction />,
  },
];

export const BuildingTypesSummarySection = () => {
  const cards = cardData.map((card, index) => (
    <ServiceSummaryCard key={index} title={card.title} description={card.description} icon={card.icon} />
  ));

  return (
    <section className={styles.serviceSummarySection}>
      <Stack className={styles.serviceSummaryHeading} align="center">
        <Title order={1}>Types of Buildings We Service</Title>
        <Text className={styles.serviceSummaryDescription} size="lg" c="dimmed">
          Explore our specialized cleaning services tailored for various types of buildings. Whether it's a commercial
          space or a private facility, we ensure a spotless environment. Our approach is fully customizable, adapting to
          the specific needs and schedules of your business to provide seamless, efficient service.
        </Text>
      </Stack>
      <SimpleGrid className={styles.serviceSummaryCards} cols={{ base: 1, md: 3 }}>
        {cards}
      </SimpleGrid>
    </section>
  );
};
