import { Container, NumberInput, Select, TextInput, Textarea } from '@mantine/core';
import { Title } from '@mantine/core';
import { Footer } from '@components/Footer/Footer.component';
import { PhoneNumberInput } from '@components/PhoneNumberInput/PhoneNumberInput.component';
import styles from './Contact.module.css';

export const Contact = () => {
  return (
    <Container size="xl" className={styles.contact}>
      <div className={styles.formWrapper}>
        <Title order={1}>Get in touch</Title>
        <TextInput className={styles.input} size="md" radius="md" label="Full Name" withAsterisk required />
        <TextInput className={styles.input} size="md" radius="md" label="Email" withAsterisk required />
        <PhoneNumberInput
          className={styles.input}
          size="md"
          radius="md"
          type="tel"
          withAsterisk
          required
          label="Phone Number"
        />
        <TextInput className={styles.input} size="md" radius="md" withAsterisk required label="Business Name" />
        <NumberInput className={styles.input} size="md" radius="md" hideControls label="Approximate Square Footage" />
        <Select
          className={styles.input}
          size="md"
          radius="md"
          label="Cleanings per week"
          data={['1', '2', '3', '4', '5', '6', '7', 'Other']}
        />
        <Textarea
          className={styles.input}
          size="md"
          radius="md"
          autosize
          minRows={6}
          resize="vertical"
          label="Tell us more"
          description="Tell us more about your company and what sort of solutions you are looking for.  What are your concerns (current and past) and how can we help?"
          withAsterisk
          required
        />
      </div>
      <Footer />
    </Container>
  );
};
