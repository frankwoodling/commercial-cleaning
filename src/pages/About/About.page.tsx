import { Container, Text } from '@mantine/core';
import { FeatureBanner } from '@components/FeatureBanner/FeatureBanner.subcomponent';
import { Footer } from '@components/Footer/Footer.component';
import { Hero } from '@components/Hero/Hero.component';
import {
  HeroButtons,
  HeroDescriptions,
  HeroImages,
  HeroLinks,
  HeroPills,
  HeroTitles,
} from '@components/Hero/Hero.types';
import { COMPANY_NAME } from '@constants/company.constants';
import styles from './About.module.css';
import { AboutOfferingSection } from './AboutOfferingSection.subcontainer';

const AboutFeatureContent = () => {
  return (
    <div className={styles.aboutFeatureContent}>
      <Text size="xl">
        At {COMPANY_NAME}, our foundation is built on the strong family values that guide every aspect of our business.
        We bring a personal touch to the professional cleaning industry, treating every client's space as if it were our
        own. Our commitment to excellence is deeply rooted in our family ethos, which emphasizes respect, integrity, and
        dedication.
      </Text>
      <Text size="xl">
        We pride ourselves on using the best practices in the industry combined with a personal touch only a family
        business can offer. From eco-friendly products to meticulous attention to detail, discover how we make your
        spaces shine while caring for the environment.
      </Text>
      <Text size="xl">
        As we grow, we continue to maintain the close-knit feel of a family business while expanding our capabilities to
        serve a wider range of clients. Join us in experiencing a cleaning service that cares deeply about creating a
        clean, positive environment for you to thrive.
      </Text>
    </div>
  );
};

export const About = () => {
  return (
    <Container size="xl" className={styles.about}>
      <Hero
        pillText={HeroPills.About}
        titleText={HeroTitles.About}
        description={HeroDescriptions.About}
        buttonText={HeroButtons.About}
        buttonLink={HeroLinks.About}
        imageUrl={HeroImages.About}
      />
      <FeatureBanner
        reverseOrder
        content={<AboutFeatureContent />}
        imageUrl="https://res.cloudinary.com/dwrctul3s/image/upload/v1722279822/commercial_cleaning/about_feature_v1_z1vpzn.png"
      />
      <AboutOfferingSection />
      <Footer />
    </Container>
  );
};
