import {
  BiCheckShield,
  BiCustomize,
  BiMessageSquareDetail,
  BiShieldQuarter,
  BiTimeFive,
  BiUserCheck,
} from 'react-icons/bi';
import { Link } from 'react-router-dom';
import { Button, SimpleGrid, Stack, Title } from '@mantine/core';
import { FeatureCard, FeatureCardProps } from '@components/FeatureCard/FeatureCard.component';
import { RoutesEnum } from '@routes/AppRoutes.types';
import styles from './About.module.css';

const cardData: FeatureCardProps[] = [
  {
    title: 'Licensed and Insured',
    description:
      "Rest easy knowing you're in safe hands with our fully licensed and insured services. We adhere to the highest standards to protect you and your property during every cleaning session.",
    icon: BiShieldQuarter,
  },
  {
    title: 'Flexibility',
    description:
      'Your schedule is our priority. We offer flexible cleaning services that can be customized to fit your time constraints and frequency needs, ensuring minimal disruption to your daily operations.',
    icon: BiTimeFive,
  },
  {
    title: 'Reliability',
    description:
      'Depend on us to be there when you need us. Our commitment to punctuality and consistent quality means you can trust us to maintain your spaces efficiently and effectively.',
    icon: BiCheckShield,
  },
  {
    title: 'Vetted Staff',
    description:
      'Our team is our pride. Each member undergoes a thorough background check and training to ensure they meet our high standards of professionalism and expertise.',
    icon: BiUserCheck,
  },
  {
    title: 'Customized Cleaning Solutions',
    description:
      "No one-size-fits-all here. We provide tailored cleaning plans that address the specific needs of your space, whether it's a small office or a large commercial facility.",
    icon: BiCustomize,
  },
  {
    title: 'Responsive Customer Service',
    description:
      'We believe in open lines of communication. Our responsive customer service team is always ready to answer your questions and resolve any issues quickly and courteously.',
    icon: BiMessageSquareDetail,
  },
];

export const AboutOfferingSection = () => {
  const cards = cardData.map((card, index) => (
    <FeatureCard key={index} title={card.title} description={card.description} icon={card.icon} />
  ));

  return (
    <section className={styles.serviceSummarySection}>
      <Stack className={styles.serviceSummaryHeading} align="center">
        <Title order={1}>What we offer</Title>
      </Stack>
      <SimpleGrid className={styles.serviceSummaryCards} cols={{ base: 1, md: 2 }}>
        {cards}
      </SimpleGrid>
      <div className={styles.buttonContainer}>
        <Link to={RoutesEnum.Services}>
          <Button size="xl">See our services</Button>
        </Link>
      </div>
    </section>
  );
};
