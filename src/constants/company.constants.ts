export const COMPANY_NAME = 'Complete Commercial Cleaners';
export const COMPANY_EMAIL = 'info@completecommercialcleaners.com';
export const COMPANY_PHONE = '(123) 234-4567';
export const COMPANY_SERVICE_AREAS = [
  'South Kansas City',
  'Overland Park',
  'Leawood',
  'Grandview',
  'Belton',
  "Lee's Summit",
  'Blue Springs',
  'Independence',
];
