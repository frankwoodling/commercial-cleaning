import { Link } from 'react-router-dom';
import { Box, Button, Center } from '@mantine/core';
import { RoutesEnum } from '@routes/AppRoutes.types';

type FooterLink = {
  label: string;
  route: RoutesEnum;
};

const footerLinks: FooterLink[] = [
  { label: 'Home', route: RoutesEnum.Home },
  { label: 'Services', route: RoutesEnum.Services },
  { label: 'About', route: RoutesEnum.AboutUs },
  { label: 'Contact Us', route: RoutesEnum.ContactUs },
];

export const FooterLinks = () => {
  return (
    <Box>
      <Center>
        {footerLinks.map((link, index) => (
          <Link key={index} to={link.route}>
            <Button size="compact-sm" variant="subtle">
              {link.label}
            </Button>
          </Link>
        ))}
      </Center>
    </Box>
  );
};
