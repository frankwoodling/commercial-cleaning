import { BiUpArrowAlt } from 'react-icons/bi';
import { ActionIcon, Affix, Anchor, Divider, Grid, Group, Space, Stack, Text, Title, Transition } from '@mantine/core';
import { useWindowScroll } from '@mantine/hooks';
import { ServiceAreas } from '@components/ServiceAreaList/ServiceAreaList.component';
import { COMPANY_EMAIL, COMPANY_NAME, COMPANY_PHONE } from '@constants/company.constants';
import styles from './Footer.module.css';
import { FooterLinks } from './FooterLinks.component';

export const Footer = () => {
  const [scroll, scrollTo] = useWindowScroll();

  return (
    <section className={styles.footer}>
      <Grid gutter={'var(--mantine-spacing-xl)'} className={styles.infoGrid}>
        <Grid.Col span={{ base: 12, lg: 4 }}>
          <Title order={1} className={styles.footerTitle}>
            Crafting Spotless Spaces & Impeccable Experiences
          </Title>
        </Grid.Col>

        <Grid.Col span={{ base: 12, md: 6, lg: 4 }}>
          <ServiceAreas />
        </Grid.Col>

        <Grid.Col span={{ base: 12, md: 6, lg: 4 }}>
          <Stack className={styles.list} justify="flex-start" gap="var(--mantine-spacing-sm)">
            <Title order={3} fw={700}>
              Contact
            </Title>
            <Text>Email</Text>
            <Anchor href={`mailto:${COMPANY_EMAIL}`}>{COMPANY_EMAIL}</Anchor>
            <Space h="sm" />
            <Text>Phone</Text>
            <Anchor href={`tel:${COMPANY_PHONE}`}>{COMPANY_PHONE}</Anchor>
          </Stack>
        </Grid.Col>
      </Grid>
      <Divider className={styles.divider} size="xs" />

      <Group className={styles.footerLinks} justify="center">
        <FooterLinks />
        {/* <SocialMediaGroup /> */}

        <Text c="dimmed" size="md">
          © 2024 {COMPANY_NAME}. All Rights Reserved.
        </Text>
      </Group>

      <Affix position={{ bottom: '1rem', right: '1rem' }}>
        <Transition transition="slide-up" mounted={scroll.y > 0}>
          {(transitionStyles) => (
            <ActionIcon size="md" variant="light" style={transitionStyles} onClick={() => scrollTo({ y: 0 })}>
              <BiUpArrowAlt className={styles.icon} />
            </ActionIcon>
          )}
        </Transition>
      </Affix>
    </section>
  );
};
