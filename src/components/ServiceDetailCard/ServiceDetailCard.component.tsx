import { Badge, Button, Card, Group, Image, Text, Title } from '@mantine/core';
import classes from './ServiceDetailCard.module.css';

export type ServiceDetailCardProps = {
  title: string;
  description: string;
  image: string;
  badges?: { emoji: string; label: string }[];
  detailsUrl?: string; // link to details page for a service
};

export const ServiceDetailCard = ({ title, description, image, badges, detailsUrl }: ServiceDetailCardProps) => {
  const features = badges?.map((badge, index) => (
    <Badge variant="light" key={index} leftSection={badge.emoji}>
      {badge.label}
    </Badge>
  ));

  return (
    <Card withBorder className={classes.card}>
      <Card.Section>
        <Image src={image} alt={title} h={400} />
      </Card.Section>

      <div className={classes.content}>
        {features && (
          <Card.Section>
            <Group>{features}</Group>
          </Card.Section>
        )}

        <Card.Section>
          <Group justify="apart">
            <Title order={2} fw={700}>
              {title}
            </Title>
          </Group>
          <Text>{description}</Text>
        </Card.Section>
      </div>

      {detailsUrl && (
        <div className={classes.buttonContainer}>
          <Button size="md">Show details</Button>
        </div>
      )}
    </Card>
  );
};
