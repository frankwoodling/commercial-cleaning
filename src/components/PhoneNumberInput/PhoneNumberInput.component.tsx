import { useState } from 'react';
import { TextInput, TextInputProps } from '@mantine/core';

const formatPhoneNumber = (value: string) => {
  if (!value) return value;

  const phoneNumber = value.replace(/[^\d]/g, '');
  const phoneNumberLength = phoneNumber.length;

  if (phoneNumberLength < 4) return phoneNumber;
  if (phoneNumberLength < 7) {
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
  }
  return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3, 6)}-${phoneNumber.slice(6, 10)}`;
};

export const PhoneNumberInput = ({ value, onChange, ...props }: TextInputProps) => {
  const [phone, setPhone] = useState(value || '');

  const handlePhoneValueChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const input = event.target.value;
    const formattedPhone = formatPhoneNumber(input);
    setPhone(formattedPhone);
    if (onChange) onChange(event);
  };

  return (
    <TextInput value={phone} onChange={handlePhoneValueChange} type="tel" placeholder="(123) 456-7890" {...props} />
  );
};
