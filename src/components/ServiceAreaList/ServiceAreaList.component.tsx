import { Stack, Text, Title } from '@mantine/core';
import { COMPANY_SERVICE_AREAS } from '../../constants/company.constants';
import styles from './ServiceAreaList.module.css';

export const ServiceAreas = () => {
  return (
    <Stack className={styles.list} gap="var(--mantine-spacing-sm)">
      <Title order={3} fw={700}>
        Service Areas
      </Title>
      {COMPANY_SERVICE_AREAS.map((area) => (
        <Text key={area}>{area}</Text>
      ))}
    </Stack>
  );
};
