import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

/**
 * Without this routing will cause the page position to remain in the same location after clicking a link.
 * This will ensure page goes to the top on all route changes.
 */
export const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};
