import { BiLogoFacebookCircle, BiLogoInstagram } from 'react-icons/bi';
import { BsTwitterX } from 'react-icons/bs';
import { ActionIcon, Anchor, Group } from '@mantine/core';
import { FACEBOOK_LINK, INSTAGRAM_LINK, TWITTER_LINK } from '../../constants/socials.constants';
import styles from './SocialMediaGroup.module.css';

type SocialLink = {
  href: string;
  icon: JSX.Element;
};

const socialLinks: SocialLink[] = [
  { href: `https://${FACEBOOK_LINK}`, icon: <BiLogoFacebookCircle className={styles.icon} /> },
  { href: `https://${INSTAGRAM_LINK}`, icon: <BiLogoInstagram className={styles.icon} /> },
  { href: `https://${TWITTER_LINK}`, icon: <BsTwitterX className={styles.icon} /> },
];

export const SocialMediaGroup = () => {
  return (
    <Group gap="var(--mantine-spacing-sm)">
      {socialLinks.map((link, index) => (
        <Anchor key={index} href={link.href} target="_blank">
          <ActionIcon size="lg" color="gray" variant="outline">
            {link.icon}
          </ActionIcon>
        </Anchor>
      ))}
    </Group>
  );
};
