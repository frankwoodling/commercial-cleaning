import { Link, useNavigate } from 'react-router-dom';
import { AppShell, Burger, Button, Group } from '@mantine/core';
import { RoutesEnum } from '@routes/AppRoutes.types';
import styles from './Header.module.css';
import placeholderLogo from '/logoipsum-330.svg';

type HeaderProps = {
  isOpen: boolean;
  toggleMobileNav: () => void;
  setRef: (ref: HTMLButtonElement | null) => void;
};

export const Header = ({ isOpen = false, toggleMobileNav, setRef }: HeaderProps) => {
  const navigate = useNavigate();

  return (
    <AppShell.Header withBorder={false} className={styles.header}>
      <Group>
        <Group justify="space-between" style={{ flex: 1 }}>
          <img
            src={placeholderLogo}
            onClick={() => navigate(RoutesEnum.Home)}
            style={{ cursor: 'pointer', width: '150px' }}
          />
          <Group gap="xl" visibleFrom="md">
            <Link to={RoutesEnum.Home}>
              <Button size="lg" variant="subtle">
                Home
              </Button>
            </Link>
            <Link to={RoutesEnum.Services}>
              <Button size="lg" variant="subtle">
                Services
              </Button>
            </Link>
            <Link to={RoutesEnum.AboutUs}>
              <Button size="lg" variant="subtle">
                About
              </Button>
            </Link>
            <Link to={RoutesEnum.ContactUs}>
              <Button size="lg" variant="filled">
                Get Quote!
              </Button>
            </Link>
          </Group>
          <Burger ref={setRef} opened={isOpen} onClick={toggleMobileNav} hiddenFrom="md" size="sm" />
        </Group>
      </Group>
    </AppShell.Header>
  );
};
