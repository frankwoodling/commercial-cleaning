import { IconType } from 'react-icons';
import { Card, Group, Stack, Text, ThemeIcon, Title } from '@mantine/core';
import styles from './FeatureCard.module.css';

export type FeatureCardProps = {
  title: string;
  description: string;
  icon: IconType;
};

export const FeatureCard = ({ title, description, icon: IconComponent }: FeatureCardProps) => {
  return (
    <Card className={styles.card}>
      <Group wrap="nowrap" align="flex-start">
        <ThemeIcon size={64}>{IconComponent && <IconComponent className={styles.icon} />}</ThemeIcon>
        <Stack>
          <Title order={2}>{title}</Title>
          <Text>{description}</Text>
        </Stack>
      </Group>
    </Card>
  );
};
