export enum FeatureBannerImages {
  Landing = 'https://res.cloudinary.com/dwrctul3s/image/upload/v1721541086/commercial_cleaning/landing_feature_v1_solid_bks06f.png',
  About = 'https://res.cloudinary.com/dwrctul3s/image/upload/v1722279822/commercial_cleaning/about_feature_v1_z1vpzn.png',
}
