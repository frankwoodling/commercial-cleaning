import { ReactElement } from 'react';
import { Flex, Image } from '@mantine/core';
import { ListCard } from '@components/ListCard/ListCard.subcontainer';
import styles from './FeatureBanner.module.css';

type ContentComponent = typeof ListCard;

type FeatureBannerProps = {
  content: ReactElement<ContentComponent>;
  imageUrl: string;
  reverseOrder?: boolean;
};

export const FeatureBanner = ({ content, imageUrl, reverseOrder = false }: FeatureBannerProps) => {
  return (
    <section>
      <Flex className={styles.featureWrapper} direction={{ base: 'column', lg: reverseOrder ? 'row-reverse' : 'row' }}>
        <div className={styles.content}>{content}</div>
        <Image className={styles.image} src={imageUrl} />
      </Flex>
    </section>
  );
};
