import { Link } from 'react-router-dom';
import { Button, Image, Pill, Text, Title } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import styles from './Hero.module.css';

type HeroProps = {
  pillText?: string;
  titleText: React.ReactNode;
  description?: React.ReactNode;
  buttonText?: string;
  buttonLink?: string;
  imageUrl: string;
};

export const Hero = ({ pillText, titleText, description, buttonText, buttonLink, imageUrl }: HeroProps) => {
  // TODO: Need to be able to access this globally and use postcss variables
  const isSmallScreen = useMediaQuery('(max-width: 62em)');

  return (
    <div className={styles.heroWrapper}>
      <div className={styles.heroContent}>
        {pillText && <Pill size={isSmallScreen ? 'md' : 'xl'}>{pillText}</Pill>}
        <div className={styles.heroContentTextWrapper}>
          {titleText && (
            <Title className={styles.heroText} order={1}>
              {titleText}
            </Title>
          )}
          {description && (
            <Text className={styles.heroText} size="lg">
              {description}
            </Text>
          )}
        </div>
        {buttonText && buttonLink && (
          <Link to={buttonLink}>
            <Button size="lg" variant="filled">
              {buttonText}
            </Button>
          </Link>
        )}
      </div>
      {imageUrl && <Image fit="contain" src={imageUrl} />}
    </div>
  );
};
