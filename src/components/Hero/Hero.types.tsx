export enum HeroPills {
  Landing = 'Commercial Cleaning & Janitorial Service',
  About = 'Your Trusted Cleaning Partners',
  Services = 'Comprehensive Cleaning Services',
}

export enum HeroTitles {
  Landing = 'Expert Cleaning, \nExceptional Results',
  About = 'Family Values, Professional Standards',
  Services = 'Tailored Cleaning Solutions for Every Space',
}

export enum HeroDescriptions {
  Landing = 'Achieve pristine conditions across all your properties with our expert cleaning services. We cater to diverse sectors, ensuring a customized approach that addresses the unique needs of each space, from offices to post construction and more.',
  About = 'Get to know our family-owned business where every client is treated like part of the family. We bring personal care and professional expertise to every job, ensuring your space is not just clean but cared for.',
  Services = 'Explore our wide range of cleaning services designed to meet the unique demands of various sectors. Whether you need routine janitorial services, meticulous deep cleaning, or specialized post-construction cleanup, our team is equipped to handle it all. Get the highest standard of cleanliness with our dedicated professionals.',
}

export enum HeroButtons {
  Landing = 'See Our Services',
  About = 'Contact Us',
  Services = 'Get a Quote',
}

export enum HeroLinks {
  Landing = '/services',
  About = '/contact-us',
  Services = '/quote',
}

export enum HeroImages {
  Landing = 'https://res.cloudinary.com/dwrctul3s/image/upload/v1721282084/commercial_cleaning/landing_hero_v1_ovmufs.png',
  About = 'https://res.cloudinary.com/dwrctul3s/image/upload/v1722281431/commercial_cleaning/about_hero_v1_agzxam.png',
  Services = 'https://res.cloudinary.com/dwrctul3s/image/upload/v1721418873/commercial_cleaning/services_v1_vrk6rf.png',
}
