import { BiBuildings, BiHome, BiPhone, BiUserCircle } from 'react-icons/bi';
import { Link } from 'react-router-dom';
import { AppShell, Text, ThemeIcon, UnstyledButton } from '@mantine/core';
import { RoutesEnum } from '../../routes/AppRoutes.types';
import styles from './MobileNav.module.css';

interface MobileNavProps {
  toggleMobileNav: () => void;
  setRef: (ref: HTMLDivElement | null) => void;
}

export const MobileNav = ({ toggleMobileNav, setRef }: MobileNavProps) => {
  return (
    <AppShell.Navbar className={styles.navbar} ref={setRef} p="md" h="fit-content">
      <Link to={RoutesEnum.Home} className={styles.link} onClick={toggleMobileNav}>
        <ThemeIcon size={36} radius="md" variant="filled">
          <BiHome className={styles.icon} />
        </ThemeIcon>
        <UnstyledButton>
          <Text className={styles.linkText}>Home</Text>
        </UnstyledButton>
      </Link>
      <Link to={RoutesEnum.Services} className={styles.link} onClick={toggleMobileNav}>
        <ThemeIcon size={36} radius="md" variant="filled">
          <BiBuildings className={styles.icon} />
        </ThemeIcon>
        <UnstyledButton>
          <Text className={styles.linkText}>Services</Text>
        </UnstyledButton>
      </Link>
      <Link to={RoutesEnum.AboutUs} className={styles.link} onClick={toggleMobileNav}>
        <ThemeIcon size={36} radius="md" variant="filled">
          <BiUserCircle className={styles.icon} />
        </ThemeIcon>
        <UnstyledButton>
          <Text className={styles.linkText}>About</Text>
        </UnstyledButton>
      </Link>
      <Link to={RoutesEnum.ContactUs} className={styles.link} onClick={toggleMobileNav}>
        <ThemeIcon size={36} radius="md" variant="filled">
          <BiPhone className={styles.icon} />
        </ThemeIcon>
        <UnstyledButton>
          <Text className={styles.linkText}>Contact Us</Text>
        </UnstyledButton>
      </Link>
    </AppShell.Navbar>
  );
};
