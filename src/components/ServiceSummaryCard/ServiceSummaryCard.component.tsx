import React, { ReactElement } from 'react';
import { Card, Text, ThemeIcon, Title } from '@mantine/core';
import styles from './ServiceSummaryCard.module.css';

export type ServiceSummaryCardProps = {
  title: string;
  description: string;
  icon?: ReactElement;
};

export const ServiceSummaryCard = ({ title, description, icon }: ServiceSummaryCardProps) => {
  return (
    <Card className={styles.card} shadow="sm">
      <div className={styles.topContainer}>
        <ThemeIcon size={60} radius="md" variant="filled">
          {icon ? React.cloneElement(icon, { className: styles.icon }) : null}
        </ThemeIcon>
        <Title order={2} fw={600}>
          {title}
        </Title>
      </div>
      <Text c="dimmed">{description}</Text>
    </Card>
  );
};
