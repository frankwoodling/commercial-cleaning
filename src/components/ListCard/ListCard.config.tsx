export type ListCardConfigType = {
  title: string;
  description: string;
  listItems: string[];
  buttonText: string;
};

// TODO: This pattern is different from the other enums.
export const ListCardConfig: Record<string, ListCardConfigType> = {
  landing: {
    title: 'Exceptional Cleaning Standards & Tailored Service',
    description:
      'Experience the difference with our cleaning services that go beyond the basics. We focus on what really matters to you, ensuring meticulous attention to detail and a customized approach for every client.',
    listItems: [
      'Customized Cleaning Plans: Adapted to meet the specific needs of your facility.',
      'Advanced Cleaning Technologies: Utilizing the latest equipment and eco-friendly products.',
      'Trained and Trustworthy Staff: Professional cleaners who are rigorously trained and vetted.',
      'Comprehensive Coverage: Serving a wide range of industries including offices, retail, and hospitality.',
    ],
    buttonText: 'Get a Quote!',
  },
};
