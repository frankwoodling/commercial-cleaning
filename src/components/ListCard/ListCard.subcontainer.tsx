import { BiBadgeCheck } from 'react-icons/bi';
import { Button, Card, Group, List, Text, Title } from '@mantine/core';
import { ListCardConfig } from './ListCard.config';
import styles from './ListCard.module.css';

type ListCardProps = {
  type: keyof typeof ListCardConfig;
};

export const ListCard = ({ type }: ListCardProps) => {
  const config = ListCardConfig[type];

  return (
    <Card className={styles.card}>
      <Group>
        <Title order={1}>{config.title}</Title>
        <Text size="md" c="dimmed">
          {config.description}
        </Text>
      </Group>

      <List
        spacing="xs"
        size="md"
        center
        icon={<BiBadgeCheck style={{ color: '#228be6', width: '36px', height: '36px' }} />}
      >
        {config.listItems.map((item, index) => (
          <List.Item key={index}>{item}</List.Item>
        ))}
      </List>

      <Button size="lg">{config.buttonText}</Button>
    </Card>
  );
};
